#pragma once

#include <SDL/SDL.h>
#include <GL/glew.h>
#include <vector>

#include <Gamengine/Gamengine.h>
#include <Gamengine/GLTexture.h>
#include <Gamengine/GLSLProgram.h>
#include <Gamengine/Sprite.h>
#include <Gamengine/Window.h>
#include <Gamengine/Camera2D.h>
#include <Gamengine/SpriteBatch.h>
#include <Gamengine/InputManager.h>
#include <Gamengine/Timing.h>

#include "Bullet.h"

enum class GameState {PLAY, EXIT};

class MainGame
{
public:
	MainGame();
	~MainGame();

	void run();

private:
	void initSystems();
	void initShaders();
	void gameLoop();
	void processInput();
	void drawGame();
	void calculateFPS();

	int _screenWidth;
	int _screenHeight;
	GameState _gameState;

	Gamengine::Window _window;
	Gamengine::GLSLProgram _colorProgram;
	Gamengine::Camera2D _camera;
	Gamengine::SpriteBatch _spriteBatch;
	Gamengine::InputManager _inputManager;
	Gamengine::fpsLimiter _fpsLimiter;
	Gamengine::ColorRGBA8 _backgroundColor;

	std::vector<Bullet> _bullets;
	

	float _maxFPS;
	float _time;
	float _fps;
};

