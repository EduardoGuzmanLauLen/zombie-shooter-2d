#pragma once
#include <glm/glm.hpp>
#include <Gamengine/SpriteBatch.h>

class Bullet
{
public:
	Bullet(glm::vec2 pos, glm::vec2 dir, float speed, int lifeTime);
	~Bullet();
	void draw(Gamengine::SpriteBatch& spriteBatch);
	bool update();

private:
	float _speed;
	int _lifeTime;
	glm::vec2 _direction;
	glm::vec2 _position;
};

