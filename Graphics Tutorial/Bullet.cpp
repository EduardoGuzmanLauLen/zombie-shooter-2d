#include "Bullet.h"
#include <Gamengine/ResourceManager.h>
#include <Gamengine/SpriteBatch.h>


Bullet::Bullet(glm::vec2 pos, glm::vec2 dir, float speed, int lifeTime)
{
	_lifeTime = lifeTime;
	_position = pos;
	_direction = dir;
	_speed = speed;
}


Bullet::~Bullet()
{
}


void Bullet::draw(Gamengine::SpriteBatch& spriteBatch) {
	glm::vec4 uv(0.0f, 0.0f, 1.0f, 1.0f);
	static Gamengine::GLTexture texture = Gamengine::ResourceManager::getTexture("Textures/jimmyJump_pack/PNG/CharacterRight_Standing.png");
	Gamengine::ColorRGBA8 color = Gamengine::ColorRGBA8(255, 255, 255, 255);
	glm::vec4 positionAndSize = glm::vec4(_position.x, _position.y, 30, 30);
	spriteBatch.draw(positionAndSize, uv, texture.id, 0.0f, color);
}

bool Bullet::update() {
	_position += _direction * _speed;
	_lifeTime--;
	if (_lifeTime == 0)
		return true;
	return false;
}
