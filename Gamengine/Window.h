#pragma once

#include <SDL/SDL.h>
#include <GL/glew.h>
#include "Vertex.h"

#include <string>
namespace Gamengine {
	enum WindowFlags { INVISABLE = 0x1, FULLSCREEN = 0x2, BORDERLESS = 0x3 };

	class Window
	{
	public:
		Window();
		~Window();

		int createWindow(std::string windowName, int screenWidth, int screenHeight, const ColorRGBA8& color, unsigned int currentFlags);
		void swapBuffer();
		int getScreenWidth() { _screenWidth; }
		int getScreenHeight() { _screenHeight; }

	private:
		SDL_Window* _sdlWindow;
		int _screenWidth, _screenHeight;
	};
}