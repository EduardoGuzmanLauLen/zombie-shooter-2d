#include "Window.h"
#include "GamengineErrors.h"


namespace Gamengine {
	Window::Window()
	{
	}


	Window::~Window()
	{
	}

	int Window::createWindow(std::string windowName, int screenWidth, int screenHeight, const ColorRGBA8& color, unsigned int currentFlags){

		Uint32 flags = SDL_WINDOW_OPENGL;

		if (currentFlags & INVISABLE)
			flags |= SDL_WINDOW_HIDDEN;
		if (currentFlags & FULLSCREEN)
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		if (currentFlags & BORDERLESS)
			flags |= SDL_WINDOW_BORDERLESS;

		_sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
		if (_sdlWindow == nullptr)
			fatalError("SDL Window could not be created");

		SDL_GLContext contextGL = SDL_GL_CreateContext(_sdlWindow);
		if (contextGL == nullptr)
			fatalError("SDL GL context could not be created");

		GLenum errorGL = glewInit();
		if (errorGL != GLEW_OK)
			fatalError("Could not initialize glew");

		//Check OpenGL Version
		std::printf("OpenGL Version: %s\n", glGetString(GL_VERSION));

		glClearColor((float) color.r / 255, (float) color.g / 255, (float) color.b / 255, (float) color.a / 255);

		//SET VSYNC
		SDL_GL_SetSwapInterval(1);

		//Enable alpha build
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		return 0;
	}

	void Window::swapBuffer() {
		SDL_GL_SwapWindow(_sdlWindow);
	}
}