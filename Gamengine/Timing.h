#pragma once

namespace Gamengine {

	class fpsLimiter {
	public:
		fpsLimiter();

		void setMaxFPS(float maxFPS);
		void init(float maxFPS);
		void begin();
		float end();

	private:
		void calculateFPS();

		unsigned int _startTicks;
		float _fps;
		float _frameTime;
		float _maxFPS;
	};

}