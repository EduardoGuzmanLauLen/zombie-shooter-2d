#include "Timing.h"

#include <SDL/SDL.h>

namespace Gamengine {

	fpsLimiter::fpsLimiter() {

	}

	void fpsLimiter::setMaxFPS(float maxFPS){
		_maxFPS = maxFPS;
	}

	void fpsLimiter::init(float maxFPS) {
		setMaxFPS(maxFPS);
	}

	void fpsLimiter::begin() {
		_startTicks = SDL_GetTicks();

	}

	float fpsLimiter::end() {
		calculateFPS();

		float frameTicks = SDL_GetTicks() - _startTicks;
		//Limit the FPS
		if (1000.0f / _maxFPS > frameTicks) {
			SDL_Delay(1000.0f / _maxFPS - frameTicks);
		}

		return _fps;
	}

	void fpsLimiter::calculateFPS() {
		static const int NUM_SAMPLES = 10;
		static float frameTimes[NUM_SAMPLES];
		static int currentFrame = 0;

		static float previousTicks = SDL_GetTicks();
		float currentTicks;
		currentTicks = SDL_GetTicks();

		_frameTime = currentTicks - previousTicks;
		frameTimes[currentFrame % NUM_SAMPLES] = _frameTime;
		previousTicks = currentTicks;

		int count;

		currentFrame++;
		if (currentFrame < NUM_SAMPLES)
			count = currentFrame;
		else
			count = NUM_SAMPLES;

		float frameTimeAvarage = 0;
		for (int i = 0; i < count; i++) {
			frameTimeAvarage += frameTimes[i];
		}
		frameTimeAvarage /= count;

		if (frameTimeAvarage > 0)
			_fps = 1000.0f / frameTimeAvarage;
		else
			_fps = 60.0f;
	}
}