#include "SpriteBatch.h"
#include <algorithm>

namespace Gamengine {
	SpriteBatch::SpriteBatch() : _vbo(0), _vao(0)
	{
	}


	SpriteBatch::~SpriteBatch()
	{
	}

	void SpriteBatch::init() {
		createVertexArray();
	}

	void SpriteBatch::begin(GlyphSortType sortType /* GlyphSortType::TEXTURE */) {
		_sortType = sortType;
		_renderBatches.clear();
		_glyphs.clear();
	}

	void SpriteBatch::end() {
		_glyphsPointers.resize(_glyphs.size());
		for (int i = 0; i < _glyphs.size(); i++)
			_glyphsPointers[i] = &_glyphs[i];
		sortGlyphs(); 
		createRenderBatches();
	}

	void SpriteBatch::draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint texture, float depth, const ColorRGBA8& color) {
		_glyphs.emplace_back(Glyph(destRect, uvRect, texture, depth, color));

	}

	void SpriteBatch::renderBatch() {

		glBindVertexArray(_vao);
		for (int i = 0; i < _renderBatches.size(); i++){
			glBindTexture(GL_TEXTURE_2D, _renderBatches[i].texture);

			glDrawArrays(GL_TRIANGLES, _renderBatches[i].offset, _renderBatches[i].numVertices);
		}

		glBindVertexArray(0);
	}

	void SpriteBatch::createRenderBatches(){
		std::vector <Vertex> vertices;
		vertices.resize(_glyphsPointers.size() * 6);
		if (_glyphsPointers.empty())
			return;

		int offset = 0;
		int curretVertex = 0;
		_renderBatches.emplace_back(offset, 6, _glyphsPointers[0]->texture);
		vertices[curretVertex++] = _glyphsPointers[0]->topLeft;
		vertices[curretVertex++] = _glyphsPointers[0]->bottomLeft;
		vertices[curretVertex++] = _glyphsPointers[0]->bottomRight;
		vertices[curretVertex++] = _glyphsPointers[0]->bottomRight;
		vertices[curretVertex++] = _glyphsPointers[0]->topRight;
		vertices[curretVertex++] = _glyphsPointers[0]->topLeft;
		offset += 6;

		for (int currentGlyph = 1; currentGlyph < _glyphsPointers.size(); currentGlyph++){
			if (_glyphsPointers[currentGlyph]->texture != _glyphsPointers[currentGlyph - 1]->texture)
				_renderBatches.emplace_back(offset, 6, _glyphsPointers[currentGlyph]->texture);
			else 
				_renderBatches.back().numVertices += 6;
			
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->topLeft;
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->bottomLeft;
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->bottomRight;
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->bottomRight;
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->topRight;
			vertices[curretVertex++] = _glyphsPointers[currentGlyph]->topLeft;
			offset += 6;
		}

		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		//orphan the buffer
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW);
		//upload the data
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), vertices.data());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
	}

	void SpriteBatch::createVertexArray() {
		if (_vao == 0) 
			glGenVertexArrays(1, &_vao);

		glBindVertexArray(_vao);

		if (_vbo == 0) 
			glGenBuffers(1, &_vbo);
		
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		//This is the position attribute pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));

		//This is the color attribute pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));

		//This is the UV pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));

		glBindVertexArray(0);
	}

	void SpriteBatch::sortGlyphs() {
		switch (_sortType)
		{
		case GlyphSortType::BACK_TO_FRONT:
			std::stable_sort(_glyphsPointers.begin(), _glyphsPointers.end(), compareBackToFront);
			break;
		case GlyphSortType::FRONT_TO_BACK:
			std::stable_sort(_glyphsPointers.begin(), _glyphsPointers.end(), compareFrontToBack);
			break;
		case GlyphSortType::TEXTURE:
			std::stable_sort(_glyphsPointers.begin(), _glyphsPointers.end(), compareTexture);
			break;
		}
	}

	bool SpriteBatch::compareFrontToBack(Glyph* a, Glyph* b) {
		return (a->depth < b->depth);
	}

	bool SpriteBatch::compareBackToFront(Glyph* a, Glyph* b) {
		return (a->depth > b->depth);
	}

	bool SpriteBatch::compareTexture(Glyph* a, Glyph* b) {
		return (a->texture < b->texture);
	}
}