#include "TextureCache.h"
#include "ImageLoader.h"
#include <iostream>

namespace Gamengine {
	TextureCache::TextureCache()
	{
	}


	TextureCache::~TextureCache()
	{
	}

	GLTexture TextureCache::getTexture(std::string texturePath) {
		auto mit = _textureMap.find(texturePath);

		if (mit == _textureMap.end()) {
			GLTexture newTexture = ImageLoader::loadPNG(texturePath);
			_textureMap.insert(make_pair(texturePath, newTexture));
			return newTexture;
		}
		//Returns the GLTexture which is in the second parameter of the pair
		return mit->second;
	}
}