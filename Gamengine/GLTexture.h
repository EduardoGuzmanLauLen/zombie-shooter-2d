#pragma once
#include <GL/glew.h>

namespace Gamengine {
	struct GLTexture {
		GLuint id;
		int width;
		int height;
	};
}