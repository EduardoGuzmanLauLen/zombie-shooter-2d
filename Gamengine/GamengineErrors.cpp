#include "GamengineErrors.h"

#include <iostream>
#include <cstdlib>
#include <SDL/SDL.h>

namespace Gamengine {
	void fatalError(std::string errorString) {
		std::printf("%s\n", errorString.c_str());
		std::printf("Enter any key to quit...");
		char tmp;
		std::cin >> tmp;
		SDL_Quit();
		exit(1);
	}
}