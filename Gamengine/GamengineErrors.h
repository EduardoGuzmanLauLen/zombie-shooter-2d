#pragma once
#include <string>

namespace Gamengine {
	extern void fatalError(std::string errorString);
}
