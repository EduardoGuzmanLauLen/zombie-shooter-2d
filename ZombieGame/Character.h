#pragma once
#include <glm/glm.hpp>
#include <Gamengine/SpriteBatch.h>

const float AGENT_WIDTH = 60.0f;
const float AGENT_RADIUS = AGENT_WIDTH / 2.0f;

class Zombie;
class Human;

class Character
{
public:
	Character();
	virtual ~Character();
	virtual void update(const std::vector<std::string>& levelData, std::vector<Human*>& humans, std::vector<Zombie*>& zombies, float deltaTime) = 0;
	bool collideWithLevel(const std::vector<std::string>& levelData);
	bool collideWithCharacter(Character* agent);
	void draw(Gamengine::SpriteBatch& _spriteBatch);
	bool applyDamage(float damage);
	glm::vec2 getPosition() const { return _position; }

protected:
	Gamengine::ColorRGBA8 _color;
	glm::vec2 _position;
	float _speed, _health;
	void checkTilePosition(const std::vector<std::string>& levelData, std::vector<glm::vec2>& collideTilePositions, float x, float y);
	void collideWithTile(glm::vec2 tilePosition);
};