#pragma once
#include "Character.h"
class Human : public Character
{
public:
	Human();
	virtual ~Human();
	void init(float speed, glm::vec2 position);
	virtual void update(const std::vector<std::string>& levelData, std::vector<Human*>& humans, std::vector<Zombie*>& zombies, float deltaTime) override;

private:
	glm::vec2 _direction;
	int _frames;
};

