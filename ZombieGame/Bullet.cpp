#include "Bullet.h"
#include "Human.h"
#include "Zombie.h"
#include "Character.h"
#include "Map.h"
#include <Gamengine/ResourceManager.h>

Bullet::Bullet(glm::vec2 position, glm::vec2 direction, float damage, float speed) :
_position(position), _direction(direction), _damage(damage), _speed(speed)
{
}


Bullet::~Bullet()
{
}

bool Bullet::update(const std::vector<std::string>& levelData, float deltaTime) {
	_position += _direction * _speed * deltaTime;
	return collideWithWorld(levelData);
}

void Bullet::draw(Gamengine::SpriteBatch& spriteBatch) {
	glm::vec4 destRect(_position.x + BULLET_RADIUS,
		_position.y + BULLET_RADIUS,
		BULLET_RADIUS * 2,
		BULLET_RADIUS * 2);
	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	Gamengine::ColorRGBA8 color = Gamengine::ColorRGBA8(75, 75, 75, 255);
	spriteBatch.draw(destRect, uvRect, Gamengine::ResourceManager::getTexture("Textures/circle.png").id, 0.0f, color);
}

bool Bullet::collideWithAgent(Character* character) {

	const float MIN_DISTANCE = AGENT_RADIUS + BULLET_RADIUS;

	glm::vec2 centerPositionA = _position;
	glm::vec2 centerPositionB = character->getPosition() + glm::vec2(AGENT_RADIUS);
	glm::vec2 distanceVector = centerPositionA - centerPositionB;

	float distance = glm::length(distanceVector);
	float collisionDepth = MIN_DISTANCE - distance;
	if (collisionDepth > 0) {
		return true;
	}
	return false;
}

bool Bullet::collideWithWorld(const std::vector<std::string>& levelData) {
	glm::ivec2 gridPosition;
	gridPosition.x = floor(_position.x / (float) TILE_WIDTH);
	gridPosition.y = floor(_position.y / (float)TILE_WIDTH);

	if (gridPosition.x < 0 || gridPosition.x >= levelData[0].length() || gridPosition.y < 0 || gridPosition.y >= levelData.size())
		return true;
	return (levelData[gridPosition.y][gridPosition.x] != '.');
}
