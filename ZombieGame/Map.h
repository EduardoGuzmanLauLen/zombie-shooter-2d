#pragma once
#include <vector>
#include <string>
#include <Gamengine/SpriteBatch.h>

const int TILE_WIDTH = 64;

class Map
{
public:
	Map(const std::string& filename);
	~Map();
	void draw();

	glm::vec2 getPlayerPosition() const { return _startPlayerPosition; }
	const std::vector<glm::vec2>& getZombiesPosition() const { return _startZombiePosition; }
	const std::vector<std::string>& getLevelData() const { return _levelData; }
	int getNumberOfHumans() const { return _numHumans; }
	int getWidth() const { return _levelData[0].size(); }
	int getHeight() const { return _levelData.size(); }

private:
	std::vector<std::string> _levelData;
	int _numHumans;
	Gamengine::SpriteBatch _spriteBatch;

	glm::vec2 _startPlayerPosition;
	std::vector<glm::vec2> _startZombiePosition;
};

