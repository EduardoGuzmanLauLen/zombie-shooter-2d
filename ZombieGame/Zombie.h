#pragma once
#include "Character.h"

class Zombie : public Character
{
public:
	Zombie();
	~Zombie();
	void init(float speed, glm::vec2 position);
	void update(const std::vector<std::string>& levelData, std::vector<Human*>& humans, std::vector<Zombie*>& zombies, float deltaTime) override;

private:
	Human* getNearestHuman(std::vector<Human*>& humans);
};

