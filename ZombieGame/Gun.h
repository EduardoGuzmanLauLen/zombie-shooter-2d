#pragma once
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "Bullet.h"
#include <Gamengine/AudioEngine.h>


class Gun
{
public:
	Gun(std::string name, int fireRate, int bulletsPerShot, float spread, float bulletDamage, float bulletSpeed, Gamengine::SoundEffect fireEffect);
	~Gun();
	void update(bool isMouseDown, const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets, float deltaTime);
	

private:
	std::string _name;
	int _fireRate, _bulletsPerShot;
	float _spread, _bulletSpeed, _bulletDamage, _frameCounter;
	Gamengine::SoundEffect m_fireEffect;

	void fire(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets);
};

