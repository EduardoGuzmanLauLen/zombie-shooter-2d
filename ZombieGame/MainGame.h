#pragma once

#include <Gamengine/Window.h>
#include <Gamengine/GLSLProgram.h>
#include <Gamengine/Camera2D.h>
#include <Gamengine/InputManager.h>
#include <Gamengine/ResourceManager.h>
#include <Gamengine/SpriteBatch.h>
#include <Gamengine/SpriteFont.h>
#include <Gamengine/AudioEngine.h>

#include "Map.h"
#include "Player.h"
#include "Bullet.h"

enum class GameState { PLAY, EXIT};

class Zombie;

class MainGame
{
public:
    MainGame();
    ~MainGame();

    /// Runs the game
    void run();

private:
    /// Initializes the core systems
    void initSystems();
	void initLevel(int level);

    /// Initializes the shaders
    void initShaders();

    /// Main game loop for the program
    void gameLoop();
	void updateCharacters(float deltaTime);
	void updateBullets(float deltaTime);

	void checkVictory();
    /// Handles input processing
    void processInput();

    /// Renders the game
    void drawGame();

	void drawHUD();

    /// Member Variables
    Gamengine::Window m_window; ///< The game window
    Gamengine::GLSLProgram m_textureProgram; ///< The shader program
    Gamengine::InputManager m_inputManager; ///< Handles input
    Gamengine::Camera2D m_camera; ///< Main Camera
	Gamengine::Camera2D m_HUDCamera; ///< Main Camera
	Gamengine::SpriteBatch m_charactersSpriteBatch;
	Gamengine::SpriteBatch m_HUDSpriteBatch;
	Gamengine::ResourceManager m_resourceManager;
	Gamengine::SpriteFont* m_spriteFont;
	Gamengine::AudioEngine m_audioEngine;

	std::vector<Map*> m_maps;
	Player* m_player = nullptr;
	std::vector<Human*> m_humans;
	std::vector<Zombie*> m_zombies;
	std::vector<Bullet> m_bullets;
	int m_screenWidth = 1024;
	int m_screenHeight = 768;
	int m_numHumansKilled = 0;
	int m_numZombiesKilled = 0;
	int m_currentLevel;
	float m_fps = 0.0f;
	GameState m_gamestate = GameState::PLAY;
};

