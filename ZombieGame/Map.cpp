#include "Map.h"
#include <fstream>
#include <Gamengine/GamengineErrors.h>
#include <Gamengine/ResourceManager.h>

Map::Map(const std::string& filename)
{
	std::ifstream file;
	file.open(filename);

	if (file.fail())
		Gamengine::fatalError("Failes to open " + filename);

	//Parse the first word into tmp and then the number of humers (tmp it's not neccesary in this part specifically)
	std::string tmp;
	file >> tmp >> _numHumans;

	std::getline(file, tmp);

	while (std::getline(file, tmp)) {
		_levelData.push_back(tmp);
	}
	_spriteBatch.init();
	_spriteBatch.begin();

	glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);
	Gamengine::ColorRGBA8 whiteColor = Gamengine::ColorRGBA8(255, 255, 255, 255);
	for (int y = 0; y < _levelData.size(); y++) {
		for (int x = 0; x < _levelData[y].size(); x++) {
			char tile = _levelData[y][x];
			glm::vec4 destRec(x * TILE_WIDTH, y * TILE_WIDTH, TILE_WIDTH, TILE_WIDTH);
			switch (tile)
			{
			case 'B':
			case 'R':
				_spriteBatch.draw(destRec, uvRect, Gamengine::ResourceManager::getTexture("Textures/red_bricks.png").id, 0.0f, whiteColor);
				break;
			case 'G':
				_spriteBatch.draw(destRec, uvRect, Gamengine::ResourceManager::getTexture("Textures/glass.png").id, 0.0f, whiteColor);
				break;
			case 'L':
				_spriteBatch.draw(destRec, uvRect, Gamengine::ResourceManager::getTexture("Textures/light_bricks.png").id, 0.0f, whiteColor);
				break;
			case '@':
				_levelData[y][x] = '.';
				_startPlayerPosition.x = x * TILE_WIDTH;
				_startPlayerPosition.y = y * TILE_WIDTH;
				break;
			case 'Z':
				_levelData[y][x] = '.';
				_startZombiePosition.emplace_back(x * TILE_WIDTH, y * TILE_WIDTH);
				break;
			case '.':
				break;
			default:
				std::printf("Unexpected symbol %c at (%d,%d)\n", tile, x, y);
				break;
			}
		}
	}

	_spriteBatch.end();
}


Map::~Map()
{
}

void Map::draw() {
	_spriteBatch.renderBatch();
}
