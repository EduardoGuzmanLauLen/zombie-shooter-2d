#pragma once
#include "Human.h"
#include "Bullet.h"
#include <Gamengine/InputManager.h>
#include <Gamengine/Camera2D.h>

class Gun;
class Player : public Human
{
public:
	Player();
	~Player();

	void init(float speed, glm::vec2 position, Gamengine::InputManager* inputManager, Gamengine::Camera2D* camera, std::vector<Bullet>* bullets);
	void addGun(Gun* gun );
	void update(const std::vector<std::string>& levelData, std::vector<Human*>& humans, std::vector<Zombie*>& zombies, float deltaTime) override;

private:
	Gamengine::InputManager* _inputManager;
	Gamengine::Camera2D* _camera;
	std::vector<Gun*> _guns;
	std::vector<Bullet>* _bullets;
	int _currentGunIndex;
};

