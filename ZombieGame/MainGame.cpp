#include "MainGame.h"

#include <Gamengine/Gamengine.h>
#include <Gamengine/Timing.h>
#include <Gamengine/GamengineErrors.h>
#include <SDL/SDL.h>
#include <iostream>
#include <random>
#include <ctime>
#include <algorithm>

#include "Zombie.h"
#include "Gun.h"

const float HUMAN_SPEED = 1.0f;
const float ZOMBIE_SPEED = 1.3f;
const float PLAYER_SPEED = 5.0f;

MainGame::MainGame() : m_screenWidth(1024), m_screenHeight(768), m_gamestate(GameState::PLAY), m_fps(0.0f), m_player(nullptr), m_numHumansKilled(0), m_numZombiesKilled(0) {
    // Empty
}

MainGame::~MainGame() {
	for (unsigned int i = 0; i < m_maps.size(); i++) {
		delete m_maps[i];
	}

	for (unsigned int i = 0; i < m_humans.size(); i++) {
		delete m_humans[i];
	}

	for (unsigned int i = 0; i < m_zombies.size(); i++) {
		delete m_zombies[i];
	}
}

void MainGame::run() {
	initSystems();
	initLevel(0);
	Gamengine::Music music = m_audioEngine.loadMusic("Sound/XYZ.ogg");
	music.play(-1);
	gameLoop();
}

void MainGame::initSystems() {
    // IMPLEMENT THIS!
	Gamengine::init();
	m_audioEngine.init();
	Gamengine::ColorRGBA8 backgroundColor;
	backgroundColor.r = 177;
	backgroundColor.g = 177;
	backgroundColor.b = 177;
	backgroundColor.a = 255;
	m_window.createWindow("Zombie Game", m_screenWidth, m_screenHeight, backgroundColor ,0);
	//To change the color you can use glClearColor(1.0f, 1.0f, 0.0f, 1.0f);
	initShaders();
	m_charactersSpriteBatch.init();
	m_HUDSpriteBatch.init();

	m_spriteFont = new Gamengine::SpriteFont("Fonts/chintzy.ttf", 32);
	m_camera.init(m_screenWidth, m_screenHeight);
	m_HUDCamera.init(m_screenWidth, m_screenHeight);
	m_HUDCamera.setPosition(glm::vec2(512, m_screenHeight / 2));

}

void MainGame::initLevel(int level) {
	m_maps.push_back(new Map("Levels/level1.txt"));
	m_currentLevel = level;
	
	m_player = new Player;
	m_player->init(PLAYER_SPEED, m_maps[m_currentLevel]->getPlayerPosition(), &m_inputManager, &m_camera, &m_bullets);

	m_humans.push_back(m_player);

	std::mt19937 randomEngine;
	randomEngine.seed(time(nullptr));
	std::uniform_int_distribution<int> randomPositionX(2.0f, m_maps[m_currentLevel]->getWidth() - 2 );
	std::uniform_int_distribution<int> randomPositionY(2.0f, m_maps[m_currentLevel]->getHeight() - 2 );

	for(int i = 0; i < m_maps[m_currentLevel]->getNumberOfHumans(); i++) {
		m_humans.push_back(new Human);
		glm::vec2 position(randomPositionX(randomEngine) * TILE_WIDTH, randomPositionY(randomEngine) * TILE_WIDTH);
		m_humans.back()->init(HUMAN_SPEED, position);
	}
	
	const std::vector<glm::vec2>& zombiePositions = m_maps[m_currentLevel]->getZombiesPosition();
	for (int i = 0; i < zombiePositions.size(); i++) {
		m_zombies.push_back(new Zombie);
		m_zombies.back()->init(ZOMBIE_SPEED, zombiePositions[i]);
	}

	const float BULLET_SPEED = 20.0f;
	m_player->addGun(new Gun("Magnum", 10, 1, 5.0f, 30.0f, BULLET_SPEED, m_audioEngine.loadSoundEffect("Sound/shots/pistol.wav")));
	m_player->addGun(new Gun("Shotgun", 30, 12, 20.0f, 4.0f, BULLET_SPEED, m_audioEngine.loadSoundEffect("Sound/shots/shotgun.wav")));
	m_player->addGun(new Gun("MP5", 2, 1, 10.0f, 20.0f, BULLET_SPEED, m_audioEngine.loadSoundEffect("Sound/shots/cg1.wav")));
}

void MainGame::initShaders() {
    // Compile our color shader
    m_textureProgram.compileShaders(std::string("Shaders/textureShading.vert"), std::string("Shaders/textureShading.frag"));
    m_textureProgram.addAttribute("vertexPosition");
    m_textureProgram.addAttribute("vertexColor");
    m_textureProgram.addAttribute("vertexUV");
    m_textureProgram.linkShaders();
}

void MainGame::gameLoop() {
	Gamengine::fpsLimiter _fpsLimiter;
	const float DESIRED_FPS = 120.0f;
	const int MAX_PHYSICS_STEPS = 6;
	_fpsLimiter.setMaxFPS(DESIRED_FPS);

	const float CAMERA_SCALE = 1.0f / 4.0f;
	m_camera.setScale(CAMERA_SCALE);

	const float MS_PER_SECOND = 1000.0f;
	const float DESIRED_FRAMETIME = MS_PER_SECOND / DESIRED_FPS;
	const float MAX_DELTA_TIME = 1.0f;

	float previousTicks = SDL_GetTicks();
	

	while (m_gamestate == GameState::PLAY) {
		_fpsLimiter.begin();
		float newTicks = SDL_GetTicks();
		float frameTime = newTicks - previousTicks;
		previousTicks = newTicks;
		float totalDeltaTime = frameTime / DESIRED_FRAMETIME;

		checkVictory();
		m_inputManager.update();
		processInput();

		int i = 0;
		while (totalDeltaTime > 0.0f && i < MAX_PHYSICS_STEPS) {
			float deltaTime = std::min(totalDeltaTime, MAX_DELTA_TIME);
			updateCharacters(deltaTime);
			updateBullets(deltaTime);
			totalDeltaTime -= deltaTime;
			i++;
		}
		m_camera.setPosition(m_player->getPosition());
		
		m_camera.update();
		m_HUDCamera.update();
		drawGame();

		m_fps = _fpsLimiter.end();
		std::cout << m_fps << std::endl;

	}
}

void MainGame::updateCharacters(float deltaTime) {
	//Update character positions

	for (unsigned int i = 0; i < m_humans.size(); i++) {
		m_humans[i]->update(m_maps[m_currentLevel]->getLevelData(), m_humans, m_zombies, deltaTime);
	}

	for (unsigned int i = 0; i < m_zombies.size(); i++) {
		m_zombies[i]->update(m_maps[m_currentLevel]->getLevelData(), m_humans, m_zombies, deltaTime);
	}

	//Update collisions
	for (unsigned int i = 0; i < m_humans.size(); i++) {
		for (unsigned int j = i + 1; j < m_humans.size(); j++) {
			m_humans[i]->collideWithCharacter(m_humans[j]);
		}
	}

	for (unsigned int i = 0; i < m_zombies.size(); i++) {
		//collide with other zombies
		for (unsigned int j = i + 1; j < m_zombies.size(); j++) {
			m_zombies[i]->collideWithCharacter(m_zombies[j]);
		}
		//collide with humans
		for (unsigned int j = 1; j < m_humans.size(); j++) {
			if (m_zombies[i]->collideWithCharacter(m_humans[j])) {
				m_zombies.push_back(new Zombie);
				m_zombies.back()->init(ZOMBIE_SPEED, m_humans[j]->getPosition());

				delete m_humans[j];
				m_humans[j] = m_humans.back();
				m_humans.pop_back();
			}
		}

		if (m_zombies[i]->collideWithCharacter(m_player)) {
			Gamengine::fatalError("YOU LOSE");
		}
	}
}

void MainGame::updateBullets(float deltaTime) {
	for (int i = 0; i < m_bullets.size();) {
		if (m_bullets[i].update(m_maps[m_currentLevel]->getLevelData(), deltaTime)) {
			m_bullets[i] = m_bullets.back();
			m_bullets.pop_back();
		}
		else {
			i++;
		}

	}

	bool wasBulletRemoved;

	//collide with humans
	for (int i = 0; i < m_bullets.size(); i++) {
		wasBulletRemoved = false;
		for (int j = 0; j < m_zombies.size();) {
			if (m_bullets[i].collideWithAgent(m_zombies[j])) {
				if (m_zombies[j]->applyDamage(m_bullets[i].getDamage())) {
					delete m_zombies[j];
					m_zombies[j] = m_zombies.back();
					m_zombies.pop_back();
					m_numZombiesKilled++;
				}
				else {
					j++;
				}
				m_bullets[i] = m_bullets.back();
				m_bullets.pop_back();
				wasBulletRemoved = true;
				i--;
				break;
			}
			else {
				j++;
			}
		}

		if (wasBulletRemoved == false) {
			for (int j = 1; j < m_humans.size();) {
				if (m_bullets[i].collideWithAgent(m_humans[j])) {
					if (m_humans[j]->applyDamage(m_bullets[i].getDamage())) {
						delete m_humans[j];
						m_humans[j] = m_humans.back();
						m_humans.pop_back();
						m_numHumansKilled++;
					}
					else {
						j++;
					}
					m_bullets[i] = m_bullets.back();
					m_bullets.pop_back();
					wasBulletRemoved = true;
					i--;
					break;
				}
				else {
					j++;
				}
			}
		}
	}
}

void MainGame::checkVictory() {
	if (m_zombies.empty()) {
		std::printf("*** You win! ***\n You killed %d humans and %d zombies. There are %d/%d civilians remaining",
			m_numHumansKilled, m_numZombiesKilled, m_humans.size() - 1, m_maps[m_currentLevel]->getNumberOfHumans());

		Gamengine::fatalError("");
	}
}

void MainGame::processInput() {
    SDL_Event evnt;
    //Will keep looping until there are no more events to process
    while (SDL_PollEvent(&evnt)) {
        switch (evnt.type) {
            case SDL_QUIT:
				m_gamestate = GameState::EXIT;
                break;
            case SDL_MOUSEMOTION:
                m_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
                break;
            case SDL_KEYDOWN:
                m_inputManager.pressKey(evnt.key.keysym.sym);
                break;
            case SDL_KEYUP:
                m_inputManager.releaseKey(evnt.key.keysym.sym);
                break;
            case SDL_MOUSEBUTTONDOWN:
                m_inputManager.pressKey(evnt.button.button);
                break;
            case SDL_MOUSEBUTTONUP:
                m_inputManager.releaseKey(evnt.button.button);
                break;
        }
    }
}

void MainGame::drawGame() {
    // Set the base depth to 1.0
    glClearDepth(1.0);
    // Clear the color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // IMPLEMENT THIS!
	m_textureProgram.use();

	glActiveTexture(GL_TEXTURE0);

	GLint textureUniform = m_textureProgram.getUniformLocations("mySampler");
	glUniform1i(textureUniform, 0);

	glm::mat4 projectionMatrix = m_camera.getCameraMatrix();
	GLint pUniform = m_textureProgram.getUniformLocations("P");
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

	m_maps[m_currentLevel]->draw();

	const glm::vec2 agentDimensions(AGENT_RADIUS * 2.0f);
	
	m_charactersSpriteBatch.begin();
	for (unsigned int i = 0; i < m_humans.size(); i++) {
		if (m_camera.isBoxInView(m_humans[i]->getPosition(), agentDimensions))
			m_humans[i]->draw(m_charactersSpriteBatch);
	}

	for (unsigned int i = 0; i < m_zombies.size(); i++) {
		if (m_camera.isBoxInView(m_zombies[i]->getPosition(), agentDimensions))
			m_zombies[i]->draw(m_charactersSpriteBatch);
	}

	for (int i = 0; i < m_bullets.size(); i++) {
		m_bullets[i].draw(m_charactersSpriteBatch);
	}

	m_charactersSpriteBatch.end();
	m_charactersSpriteBatch.renderBatch();

	drawHUD();

	m_textureProgram.unuse();
   
    // Swap our buffer and draw everything to the screen!
    m_window.swapBuffer();
}

void MainGame::drawHUD() {
	char buffer[256];

	glm::mat4 projectionMatrix = m_HUDCamera.getCameraMatrix();
	GLint pUniform = m_textureProgram.getUniformLocations("P");
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);
	m_HUDSpriteBatch.begin();

	sprintf_s(buffer, "Num Humans %d", m_humans.size());
	m_spriteFont->draw(m_HUDSpriteBatch, buffer, glm::vec2(0, 0), glm::vec2(1.0), 0.0f, Gamengine::ColorRGBA8(255, 255, 255, 255), Gamengine::Justification::LEFT);

	sprintf_s(buffer, "Num Zombies %d", m_zombies.size());
	m_spriteFont->draw(m_HUDSpriteBatch, buffer, glm::vec2(0, 36), glm::vec2(1.0), 0.0f, Gamengine::ColorRGBA8(255, 255, 255, 255));

	m_HUDSpriteBatch.end();
	m_HUDSpriteBatch.renderBatch();
}