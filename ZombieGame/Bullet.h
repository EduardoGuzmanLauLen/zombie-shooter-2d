#pragma once
#include <glm/glm.hpp>
#include <Gamengine/SpriteBatch.h>
#include <vector>


class Human;
class Zombie;
class Character;

const int BULLET_RADIUS = 5;
class Bullet
{
public:
	Bullet(glm::vec2 position, glm::vec2 direction, float damage, float speed);
	~Bullet();

	bool update(const std::vector<std::string>& levelData, float deltaTime);
	void draw(Gamengine::SpriteBatch& spriteBatch);
	bool collideWithAgent(Character* character);
	float getDamage() const { return _damage; }

private:
	bool collideWithWorld(const std::vector<std::string>& levelData);

	glm::vec2 _position, _direction;
	float _speed, _damage;
};

